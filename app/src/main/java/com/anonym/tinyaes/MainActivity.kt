package com.anonym.tinyaes

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val testResult = testAES()
        textView.text = if (testResult == 0) "Tests passed (successful)" else "Tests failed"


        val result = AES_ECB_encrypt("zWGkaw3WbZ8z", "Test encrypt")
        textView.text = result.toString()
    }

    private external fun testAES(): Int

    private external fun AES_ECB_encrypt(key: String, data: String)

    companion object {
        init {
            System.loadLibrary("test-aes")
        }
    }
}
